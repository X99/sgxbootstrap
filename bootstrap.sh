#!/bin/bash

sudo apt update
sudo apt upgrade -y

# pre-requisites
sudo apt-get install -y build-essential ocaml ocamlbuild automake autoconf libtool wget python libssl-dev git scons libssl-dev libcurl4-openssl-dev protobuf-compiler libprotobuf-dev debhelper cmake reprepro libssl-dev libcurl4-openssl-dev

# prepare folder to keep all the data into
mkdir SGX-Data
cd SGX-Data

# Download SDK
wget https://download.01.org/intel-sgx/sgx-linux/2.9/distro/ubuntu18.04-server/sgx_linux_x64_sdk_2.9.100.2.bin

# download driver
wget https://download.01.org/intel-sgx/sgx-linux/2.9/distro/ubuntu18.04-server/sgx_linux_x64_driver_2.6.0_95eaa6f.bin

# make them executable
chmod +x *.bin

# to install, follow install guide found here:
mkdir docs
cd docs
wget https://download.01.org/intel-sgx/sgx-linux/2.9/docs/Intel_SGX_Developer_Guide.pdf
wget https://download.01.org/intel-sgx/sgx-linux/2.9/docs/Intel_SGX_Developer_Reference_Linux_2.9_Open_Source.pdf
wget https://download.01.org/intel-sgx/sgx-linux/2.9/docs/Intel_SGX_Installation_Guide_Linux_2.9_Open_Source.pdf
wget https://download.01.org/intel-sgx/sgx-linux/2.9/docs/Intel_SGX_PSW_Release_Notes_Linux_2.9_Open_Source.pdf
wget https://download.01.org/intel-sgx/sgx-linux/2.9/docs/Intel_SGX_SDK_Release_Notes_Linux_2.9_Open_Source.pdf
cd ..

# install SGX driver
sudo ./sgx_linux_x64_driver*.bin

# Install PSW packages
echo 'deb [arch=amd64] https://download.01.org/intel-sgx/sgx_repo/ubuntu bionic main' | sudo tee /etc/apt/sources.list.d/intel-sgx.list
wget -qO - https://download.01.org/intel-sgx/sgx_repo/ubuntu/intel-sgx-deb.key | sudo apt-key add -
sudo apt update

# PSW: Install launch service:
sudo apt-get install -y libsgx-launch libsgx-urts
# PSW: Install EPID-based attestation service
sudo apt-get install -y libsgx-epid
# PSW: Install algorithm agnostic attestation service:
sudo apt-get install -y libsgx-quote-ex

# install SDK, use /opt/intel as install dir
sudo ./sgx_linux_x64_sdk_*.bin
source /opt/intel/sgxsdk/environment

# download mitigation tools
wget https://download.01.org/intel-sgx/sgx-linux/2.9/as.ld.objdump.gold.r1.tar.gz
sudo tar xzf as.ld.objdump.gold.r1.tar.gz -C /usr/local/bin

# make them all executable
sudo chmod +x -R /usr/local/bin

# copy examples to home
cp -r /opt/intel/sgxsdk/SampleCode .